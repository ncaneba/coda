#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <fstream>

#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cctype>
#define pb push_back

using namespace std;
//Server side
int main(int argc, char *argv[]){
	
    // This will execute if the input in the terminal is without a port number
    if(argc != 2){
        cout  << "Please Input Port Number!" << endl;
        exit(0);
    }
    
    int port = atoi(argv[1]);
    char msg[1500];
     
    //setup a socket and connection tools
    sockaddr_in servAddr; bzero((char*)&servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET; servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(port);

    int serverSd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSd < 0){
        cout << "Error establishing the server socket" << endl;
        exit(0);
    }
    
    //bind the socket to its local address
    int bindStatus = bind(serverSd, (struct sockaddr*) &servAddr, sizeof(servAddr));
    if(bindStatus < 0){
        cout << "Error binding socket to local address" << endl;
        exit(0);
    }
    
    // After the two if statement, it means that the server is successfully executed
    cout << "Waiting for a client to connect..." << endl;
     
    listen(serverSd, 2); sockaddr_in newSockAddr; socklen_t newSockAddrSize = sizeof(newSockAddr);
    int newSd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
    if(newSd < 0){
        cout << "Error accepting request from client!" << endl;
        exit(1);
    }

    cout << "Connected to the client!" << endl;
    string data;
    // %%%START%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
   
    // %%%%RECEIVED%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
		// Get the primary key of the client
		memset(&msg, 0, sizeof(msg));//clear the buffer
		recv(newSd, (char*)&msg, sizeof(msg), 0);
		cout << "C: " << msg << endl;
    
	
	 // %%%SEND%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

		data = " OH?";
		memset(&msg, 0, sizeof(msg)); //clear the buffer
		strcpy(msg, data.c_str());
		send(newSd, (char*)&msg, strlen(msg), 0);
		memset(&msg, 0, sizeof(msg)); //clear the buffer
		
	// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
  
    close(newSd);
    close(serverSd);
  
    return 0;   
}
