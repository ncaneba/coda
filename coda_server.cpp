#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <stdio.h>
#include <cctype>
#include <algorithm>
#include <ctime>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <fstream>

using namespace std;

#define pb push_back

// Print the deck of player and player2
void print_deck(vector<string> player,vector<string> player2){
	
	//cout << " 'Player 1:\n " << endl;
	for(int i=0; i<player.size(); i++){
		player[i][0]=='0'?
			cout << " [" << player[i][1] << player[i][2] << player[i][3] << "]" :		
			//cout << " [" << player[i][1]  << "] " :	
			cout << " [" << player[i] << "]" ;
			//cout << " [" << player[i][1] << "] " ;
	}
	cout << endl;
	
	//cout << " 'Player 2:\n " << endl;
	for(int i=0; i<player2.size(); i++){
		player2[i][0]=='0'?
			cout << " [" << player2[i][1] << player2[i][2] << player[i][3] <<  "]" :	
			//cout << " [" << player2[i][1]  << "] " :	
			cout << " [" << player2[i] << "]" ;
			//cout << " [" << player2[i][1] << "] " ;
	}
	cout << endl;	
}

// Print only one deck
void print_one_deck(vector<string> player){
	cout << " \n [Deck]:\n " << endl;
	for(int i=0; i<player.size(); i++){
		player[i][0]=='0'?
			cout << " [" << player[i][1] << player[i][2] << player[i][3] << "] " :	
			//cout << " [" << player[i][1] << "] " :	
			cout << " [" << player[i] << "] " ;
			//cout << " [" << player[i][1] << "] " ;
	}
}
// Checking if theres hypen in the deck of player
int check_hyp(vector<string> player){
	int result = 0;
	for(int i=0; i<player.size(); i++){
		if(player[i][1]=='-')
			result++;
	}
	return result;
}

bool check_num(int n){
	if(n>=1 && n<=13) 
		return true;
	else
		return false;
}

// Modify the array in the main and insert the hypen in array
void insert_hyp_func(vector<string> &player, int player_hyp,int whosplayer){
	
	if(player_hyp>0){
		cout << " \n Player! " << whosplayer << " turn to Arrange his deck, 1 to 13 Index" << endl;
		int insert_hyp;
		
		string temp_hyp;
		for(int i=0; i<player_hyp; i++){
			cout << "\n Input the index that you want to insert the " << i+1 << " hypen" << endl;  
			tele5:;
			cout << " Input: ";
			cin >> insert_hyp;  // this is an index
				if(check_num(insert_hyp)==false){
					cout << " [[PLEASE Input 1-13 only!]]\n " << endl;
					goto tele5;
			}
			
			insert_hyp--;		// to make it 0 index
			temp_hyp = player[0];
		
			player.erase(player.begin()); // erase the first deck of player, it's sure a hypen character
			player.insert(player.begin()+insert_hyp,temp_hyp);
			
			//print_one_deck(player);
			
			cout << endl;
		}
	}
}

// Linear Probing Algorithm / pass by reference
void get_card_on_deck(vector<string> &deck, vector<string> &player, vector<string> &player2){
	
	// Linear Probing
	// cout << "[ Linear Probing ]" << endl;
	int ctr=0;
	int binary=0;
	while(1){
		int index = rand()%deck.size();
		// cout << "Generated Random #: " << index << endl;
		if(deck[index]!=""){
			
			binary==0?	
				player.pb(deck[index]):
				player2.pb(deck[index]);
			
			deck[index]="";
			ctr++;
		}
		else {
			//cout << "Linear Probing->" << deck[index] <<  endl;
			while(1){
				index = (index+1)%deck.size();
				//cout << "++ " << index << ' ' << endl;
				if(deck[index]!=""){
					//cout << "++ -> " << index << ' ' << endl;
					break;
				}
			}
			
			binary==0?
				player.pb(deck[index]):
				player2.pb(deck[index]);
				
			deck[index]="";
			ctr++;
		}
		
		binary==0?
			binary=1:
			binary=0;
		
		if(ctr==deck.size())
			break;
	}
}

void clear_screen(){
	for(int i=0; i<30; i++)
		cout << endl;
}

string convert_str(string str){
	if(str=="0") 		return "00";
	else if(str=="1") 	return "01";
	else if(str=="2") 	return "02";
	else if(str=="3") 	return "03";
	else if(str=="4") 	return "04";
	else if(str=="5") 	return "05";
	else if(str=="6") 	return "06";
	else if(str=="7") 	return "07";
	else if(str=="8") 	return "08";
	else if(str=="9") 	return "09";
	else if(str=="10") 	return "10";
	else if(str=="11") 	return "11";
	else if(str=="-") 	return "0-";
	
	else
		return "ERROR";
}

int play_game(vector<string> &player, vector<string> &player2){
	
		
	int	 doubt_location, doubt_number;
	string doubt_str;
	char doubt_color;
	
	int deck_rem1;
	int deck_rem2;
	
	deck_rem1 = deck_rem2 = 13;
	
	int binary_alt = 1;
	vector<string> vec_1;
	vector<string> vec_2;
	vec_1 = player;
	vec_2 = player2;
	
	
	while(1){
			
			if(binary_alt==1){
				vec_1 = player;
				vec_2 = player2;
			}
			else{
				vec_1 = player2;
				vec_2 = player;
			}
			
			x:;
		
			if(binary_alt==1){
				for(int i=0; i<vec_1.size(); i++){
					vec_1[i][0]=='0'?
						cout << " [" << vec_1[i][1] << vec_1[i][2] << vec_1[i][3] << "]" :	
						cout << " [" << vec_1[i] << "]" ;
				}
				cout << endl;
				
				for(int i=0; i<vec_2.size(); i++){
					vec_2[i][0]=='0'?
						cout << " [" << vec_2[i][1] << vec_2[i][2] << vec_2[i][3] <<  "]" :	
						cout << " [" << vec_2[i] << "]" ;
				}
				cout << endl;	
			}
			else {
				for(int i=0; i<vec_2.size(); i++){
					vec_2[i][0]=='0'?
						cout << " [" << vec_2[i][1] << vec_2[i][2] << vec_2[i][3] <<  "]" :	
						cout << " [" << vec_2[i] << "]" ;
				}
				cout << endl;		
				
				for(int i=0; i<vec_1.size(); i++){
					vec_1[i][0]=='0'?
						cout << " [" << vec_1[i][1] << vec_1[i][2] << vec_1[i][3] << "]" :	
						cout << " [" << vec_1[i] << "]" ;
				}
				cout << endl;			
			}
			
			
			
			binary_alt==1?
				cout << " \n [ Player 1 Turn ]" << endl:
				cout << " \n [ Player 2 Turn ]" << endl;
			
			cout << "\n Please input the LOCATION of the card. Range: 1-11" << endl;
			tele:;
			cout << " Input: ";
			cin >> doubt_location;
				if(check_num(doubt_location)==false){
					cout << " [[PLEASE Input 1-13 only!]]\n " << endl;
					goto tele;
				}
			
			doubt_location--;
			
				if(vec_2[doubt_location][3]=='S'){
					cout << " [[Error, the Card is already shown!]]\n " << endl;
					goto tele;
				}			
				
			
			cout << " >> Please input your guessed NUMBER of that card. Range: 1-13 or '-'" << endl;
			tele3:;
			cout << " Input: ";
			cin >> doubt_str;
		
			string temp_s;
			temp_s = doubt_str;
			doubt_str = convert_str(temp_s);				
				if(doubt_str=="ERROR"){
					cout << " [[PLEASE Input 0-11 or hypen only!]]\n " << endl;
					goto tele3;
				}
			
			cout << " >> Please input your guessed COLOR of that card. Range: b or w" << endl;
			tele4:;
			cout << " Input: ";
			cin >> doubt_color;
			doubt_color = toupper(doubt_color);
				if(doubt_color=='W' || doubt_color=='B'); // <- this is semicolon, designed
				else{
					cout << " [[PLEASE Input 'b' for Black and 'w' for White only!]]\n " << endl;
					goto tele4;
				}
			
			// substr method
			string substr_guess;
			substr_guess = doubt_str;
			substr_guess += doubt_color;
			
			string substr_main = "";
			for(int i=0; i<3; i++){
				substr_main += vec_2[doubt_location][i];
			}
			
			clear_screen();
			
			int defold;
			if(substr_main==substr_guess){
				
				vec_2[doubt_location][3] = 'S';
				if(binary_alt==1)				
					deck_rem2--;	
				else
					deck_rem1--;
				
				if(deck_rem1==0){
					return 2;
				}
				else if(deck_rem2==0){
					return 1;
				}
				
				binary_alt==1?
					cout << " \n (*)Your Guess is Right, Player 2 Card have been shown" << endl:
					cout << " \n (*)Your Guess is Right, Player 1 Card have been shown" << endl;
				
						
				cout << "\n\n Choose Again Card to Guess\n" << endl;
				
				cout << " \n PLAYER 1 Remaining cards: " << deck_rem1 << endl;
				cout << " PLAYER 2 Remaining cards: " << deck_rem2 << endl << endl;
			

				goto x;
			}
			else{
				
				cout << " (*)Your guess is wrong, choose the location of the card that you want to defold " << endl;
				tele2:;
				cout << " Input: ";
				cin >> defold;
					if(check_num(defold)==false){
						cout << " [[PLEASE Input 1-13 only!]]\n " << endl;
						goto tele2;
					}
						
				defold--;
				
				if(vec_1[defold][3]=='S'){
					cout << " [[Error, the Card is already shown!]]\n " << endl;
					goto tele2;
				}
					
				vec_1[defold][3] = 'S';
				
				if(binary_alt==1)
					deck_rem1--;
				else
					deck_rem2--;
			}
			
			cout << " \n PLAYER 1 Remaining cards: " << deck_rem1 << endl;
			cout << " PLAYER 2 Remaining cards: " << deck_rem2 << endl << endl;
			
			cout << "\n\n #### GAME START ####" << endl << endl;
			
			if(binary_alt==1){
				player  = vec_1;
				player2 = vec_2;
			}
			else{
				player  = vec_2;
				player2 = vec_1;
			}
			
			binary_alt==1?
				binary_alt = 0:
				binary_alt = 1;
	}
}

//Server side
int main(int argc, char *argv[])
{
    //for the server, we only need to specify a port number
    if(argc != 2)
    {
        cerr << "Usage: port" << endl;
        exit(0);
    }
    //grab the port number
    int port = atoi(argv[1]);
    //buffer to send and receive messages with
    char msg[1500];
     
    //setup a socket and connection tools
    sockaddr_in servAddr;
    bzero((char*)&servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(port);
 
    //open stream oriented socket with internet address
    //also keep track of the socket descriptor
    int serverSd = socket(AF_INET, SOCK_STREAM, 0);
    if(serverSd < 0)
    {
        cerr << "Error establishing the server socket" << endl;
        exit(0);
    }
    //bind the socket to its local address
    int bindStatus = bind(serverSd, (struct sockaddr*) &servAddr, 
        sizeof(servAddr));
    if(bindStatus < 0)
    {
        cerr << "Error binding socket to local address" << endl;
        exit(0);
    }
    cout << "Waiting for a client to connect..." << endl;
    //listen for up to 5 requests at a time
    listen(serverSd, 5);
    //receive a request from client using accept
    //we need a new address to connect with the client
    sockaddr_in newSockAddr;
    socklen_t newSockAddrSize = sizeof(newSockAddr);
    //accept, create a new socket descriptor to 
    //handle the new connection with client
    int newSd = accept(serverSd, (sockaddr *)&newSockAddr, &newSockAddrSize);
    if(newSd < 0)
    {
        cerr << "Error accepting request from client!" << endl;
        exit(1);
    }
    cout << "Connected with client!" << endl;
    //lets keep track of the session time
    struct timeval start1, end1;
    gettimeofday(&start1, NULL);
    //also keep track of the amount of data sent as well
    int bytesRead, bytesWritten = 0;
    while(1)
    {
		/* CODA CODE SEGMENT
				srand(time(NULL)); // Generating Random #
				
				// vector player  -> player 1
				// vector player2 -> player 2 
				vector<string> player,player2,deck;
				
				// Note: the content of vector arrays are 01W or 01B 
				
				string temp="";
				for(int i=0; i<10; i++){
					temp="0"; temp += char(i+'0'); temp+='B'; temp += 'F';
					deck.pb(temp);
					temp="0"; temp += char(i+'0'); temp+='W'; temp += 'F';
					deck.pb(temp);
				}
				deck.pb("10BF"); deck.pb("10WF");
				deck.pb("11BF"); deck.pb("11WF");
				deck.pb("0-BF"); deck.pb("0-WF");
				
				// Random Shuffle the Main Deck
				random_shuffle(deck.begin(),deck.end());
				
				get_card_on_deck(deck,player,player2);
				
				//cout << " Linear Probing Result:\n " << 11
				//cout << " PRINT DECKS " << endl;
				//print_deck(player,player2);
				
				// Game Start
				
				clear_screen();
				
				// Sort the player and player2 deck
				sort(player.begin(),player.end());
				sort(player2.begin(),player2.end());
				
				cout << " \n ** Da Vinci Code **\n " << endl;
				print_deck(player,player2);
				
				// Getting the Hypen
				int player_hyp=0, player2_hyp=0;
				
				player_hyp = check_hyp(player);
				player2_hyp = check_hyp(player2);
				
				// If theres hypen in player and player2
				
				// Insert Hypen to Player 111
				insert_hyp_func(player,player_hyp,1);
				
				// Insert Hypen to Player 2
				insert_hyp_func(player2,player2_hyp,2);
				
				// NEXT is Player Guessing, TBC - to be code lol

				clear_screen();
				
				cout << "\n #### THE GAME WILL START ####" << endl << endl;

				// Function for the Game
				int win;
				win = play_game(player,player2);
				
				cout << " Player " << win << " wins!" << endl << endl << endl;
		
		 * 
		 */
        //receive a message from the client (listen)
        cout << "Awaiting client response..." << endl;
        memset(&msg, 0, sizeof(msg));//clear the buffer
        bytesRead += recv(newSd, (char*)&msg, sizeof(msg), 0);
        if(!strcmp(msg, "exit"))
        {
            cout << "Client has quit the session" << endl;
            break;
        }
        cout << "Client: " << msg << endl;
        cout << ">";
        string data;
        getline(cin, data);
        memset(&msg, 0, sizeof(msg)); //clear the buffer
        strcpy(msg, data.c_str());
        if(data == "exit")
        {
            //send to the client that server has closed the connection
            send(newSd, (char*)&msg, strlen(msg), 0);
            break;
        }
        //send the message to client
        bytesWritten += send(newSd, (char*)&msg, strlen(msg), 0);
        
    }
    //we need to close the socket descriptors after we're all done
    gettimeofday(&end1, NULL);
    close(newSd);
    close(serverSd);
    cout << "********Session********" << endl;
    cout << "Bytes written: " << bytesWritten << " Bytes read: " << bytesRead << endl;
    cout << "Elapsed time: " << (end1.tv_sec - start1.tv_sec) 
        << " secs" << endl;
    cout << "Connection closed..." << endl;
    return 0;   
}
