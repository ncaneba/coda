#include <iostream>
#include <string>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <sys/uio.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <cctype>
#include <sstream>	
using namespace std;


// Print the deck of player and player2
void print_deck(vector<string> player,vector<string> player2){
	
	cout << " Player 1:" << endl;
	for(int i=0; i<player.size(); i++){
		player[i][0]=='0'?
			cout << " [" << player[i][1] << player[i][2] << player[i][3] << "]" :		
			//cout << " [" << player[i][1]  << "] " :	
			cout << " [" << player[i] << "]" ;
			//cout << " [" << player[i][1] << "] " ;
	}
	cout << endl << endl;
	
	cout << " Player 2:" << endl;
	for(int i=0; i<player2.size(); i++){
		player2[i][0]=='0'?
			cout << " [" << player2[i][1] << player2[i][2] << player[i][3] <<  "]" :	
			//cout << " [" << player2[i][1]  << "] " :	
			cout << " [" << player2[i] << "]" ;
			//cout << " [" << player2[i][1] << "] " ;
	}
	cout << endl;	
}


string encrypt(vector<string> vec){
	string temp="";
	for(int i=0; i<vec.size(); i++){
		temp += vec[i];
	}
	return temp;
}

vector<string> tokenizer(string str){
	vector<string> temp;
	stringstream ss(str);
	ss << str;
	while(ss>>str)
		temp.push_back(str);
	
	return temp;
}

int convert_string(string str){
	int x;
	stringstream ss(str);
	ss << str;
	ss >> x;
	
	return x;
}

string convert_int(int x){
	stringstream ss;
	ss << x;
	return ss.str();
}


//Client side
int main(int argc, char *argv[]){
	
	vector<string> player,player2;
	
	
    //we need 2 things: ip address and port number, in that order
    if(argc != 3){
        cout << "Usage: ip_address port" << endl; 
        exit(0); 
    } 
    
    //grab the IP address and port number 
    char *serverIp = argv[1]; int port = atoi(argv[2]); 
    
    //create a message buffer 
    char msg[1500]; 
    
    
    //setup a socket and connection tools 
    struct hostent* host = gethostbyname(serverIp); 
    sockaddr_in sendSockAddr;   
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
    sendSockAddr.sin_family = AF_INET; 
    sendSockAddr.sin_addr.s_addr = inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
    sendSockAddr.sin_port = htons(port);
    
    int clientSd = socket(AF_INET, SOCK_STREAM, 0);
    
    //try to connect...
    int status = connect(clientSd,(sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
    
    if(status < 0){
        cout <<"Error connecting to socket!" << endl; 
        exit(0);
    }
    
    cout << "Connected to the server!" << endl;
    string data;
    // %%%START%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    // %%%%RECEIVED%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
        memset(&msg, 0, sizeof(msg));//clear the buffer
        recv(clientSd, (char*)&msg, sizeof(msg), 0);
        cout << "S: " << msg << endl;
        
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    player = tokenizer(msg);
    
    print_deck(player,player2);
    
    // %%%SEND%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
		data = " nothing";
        memset(&msg, 0, sizeof(msg));
        strcpy(msg, data.c_str());
        send(clientSd, (char*)&msg, strlen(msg), 0);
        memset(&msg, 0, sizeof(msg));
        
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
  
  
//===========================================================================================================    
    
     // %%%SEND%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
		data = "Client";
        memset(&msg, 0, sizeof(msg));
        strcpy(msg, data.c_str());
        send(clientSd, (char*)&msg, strlen(msg), 0);
        memset(&msg, 0, sizeof(msg));
        
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
	
	// %%%%RECEIVED%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
        memset(&msg, 0, sizeof(msg));//clear the buffer
        recv(clientSd, (char*)&msg, sizeof(msg), 0);
        cout << "S: " << msg << endl;
        
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    // %%%SEND%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     
		data = " ano ka na naman";
        memset(&msg, 0, sizeof(msg));
        strcpy(msg, data.c_str());
        send(clientSd, (char*)&msg, strlen(msg), 0);
        memset(&msg, 0, sizeof(msg));
        
    // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
   
    close(clientSd);

    return 0;    
}
